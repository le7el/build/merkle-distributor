// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.8.17;

import "@le7el/rewards_engine/contracts/MerkleDistributor.sol";

contract ZksyncMerkleDistributor is MerkleDistributor {
    /**
     * @dev Initialise MerkleDistributor with initial distributor settings.
     * @param _token Reward token address.
     * @param _tokenId Token id for ERC1155, 0 for ERC20.
     * @param _claimInterface API to claim the reward: 0xd9b67a26, 0x731133e9, 0x40c10f19, 0xffffffff.
     * @param _merkleRoot Merkle root of distribution.
     * @param _ipfsCid IPFS hash (without 1220 prefix) with the merkle tree data for the provided root.
     */
    constructor(address _token, uint256 _tokenId, bytes4 _claimInterface, bytes32 _merkleRoot, bytes32 _ipfsCid)
        MerkleDistributor(_token, _tokenId, _claimInterface, _merkleRoot, _ipfsCid) {}
}