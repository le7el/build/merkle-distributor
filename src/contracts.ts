export default {
  '1': {
    l7l_erc20_rewarder: "0x5682faA9D58BFfa0d348dc144995bF3DaF9666de",
    l7l_fixed_virtual_distributor_50: "0x1A20eE48c236511863D5c257018A4b73605Cfc9a",
    l7l_fixed_virtual_distributor_100: "0x67412EDF3163410947875d7a1135468a4aAa4D69"
  },
  '4': {
    merkle_distributor: "0xbA2F304b62199C979FE9d6C7EEa10b283d49C0f5",
    conditional_distributor: "0x5d014dAA8688DB97B3B65138782920faEBBb32C3",
    erc721_holder: "0xBE1eFff4F86dB8226620126B02Ba2e334d682378"
  },
  '5': {
    merkle_distributor: "0x9E7baB365BcA758681c6ee44bc38BFAf121B6a7d",
    conditional_distributor: "0xb898262910C4A585AbC8be366D6102fc77519ec7",
    erc721_holder: "0xe9589a535cbDDF6aF50a7AC162DEc1dFa1adA188",
    virtual_distributor: "0x2628D5e8fB8D95454ceE66A82Ffc512A5F14D6DC",
    one_time_offchain_tickets: "0x23Fe1Ef7c30c2007559216B2C766A9f10608d61b"
  },
  '11155111': {
    merkle_distributor: "0x5C1C7511f6d90b00bF23b12c25dA9dbD93C369B5",
    conditional_distributor: "0xC972af85DbD3d770D5E8668e1f81Fd45D52D2aaa",
    erc721_holder: "0xCF5eE082a77C5005De433e96a20CC626F29f754D",
    virtual_distributor: "0x47D3e90827dFED8Ef5d73dA83D06282e5012d82E",
    one_time_offchain_tickets: "0x49008D8c8d0f1EFe9d819Ee1AE1d11c49dDD0390",
    one_sided_staking: "0xd693f887D6Dc28D6bE8B39cc0E920c26233d3022",
    vesting_rewarder: "0x7a1a9f14c323e04e443E7F19d259393e3fa20829",
    l7l_erc20_rewarder: "0x1BA3ff8d2B4bE852e79bd8D638B16235ce794873",
    l7l_fixed_virtual_distributor_50: "0x4410D33319a1D722fD53fe70E89a6228AeC6c0c3",
    l7l_fixed_virtual_distributor_100: "0xD95E3E2A732A63dC25c73C20946ec352a0987ADD"
  },
  '137': {
    conditional_distributor: "0x276FE941757C93c4A916B985C59613692e0f551f",
    erc721_holder: "0xd373a0fDf749f8fC28B913014aFc0BE0c17490C6",
    virtual_distributor: "0x73A699D74734023aE4945FaE6205dfe383347f21",
    one_time_offchain_tickets: "0xb3E7F55d98F499c97A1DD9B585D76e10624ca429",
    one_sided_staking: "0x914A428404657CA547085Ec0Ee7Ac6f948f99A4E",
    vesting_rewarder: "0x47D3e90827dFED8Ef5d73dA83D06282e5012d82E"
  }
} as {[key: string]: {[key: string]: string}}