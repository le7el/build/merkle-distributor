import contracts from "../contracts"
import oracle from "../../artifacts/contracts/condition_oracles/ERC721Holder.sol/ERC721Holder.json"
import {initContractByAbi} from "../utils"
import {BigNumber,utils} from "ethers"

const ERC721_HOLDER_CONTRACT = 'erc721_holder'

const abi = () => {
  return oracle.abi
}

const bytecode = () => {
  return oracle.bytecode
}

const deployedAddress = (network : string | number) => {
  if (!contracts[`${network}`]) return null
  return contracts[`${network}`][ERC721_HOLDER_CONTRACT]
}

const initContract = (contractKey : string, readOnly = true, web3Provider = null) => {
  return initContractByAbi(oracle.abi, contractKey, readOnly, web3Provider)
}

const owner = (web3Provider = null, contractKey = ERC721_HOLDER_CONTRACT) => {
  return initContract(contractKey, true, web3Provider)
    .then(contract => contract.owner())
}

const getReward = (nftId : number | BigNumber, web3Provider = null, contractKey = ERC721_HOLDER_CONTRACT) => {
  return initContract(contractKey, true, web3Provider)
    .then(contract => contract.getReward(nftId))
}

const hasClaim = (account : string, claim : string, web3Provider = null, contractKey = ERC721_HOLDER_CONTRACT) => {
  return initContract(contractKey, true, web3Provider)
    .then(contract => contract.hasClaim(account, claim))
}

const prepareClaim = (
  claimInterface : string,
  rewardToken : string,
  rewardTokenId : number | BigNumber,
  nftId : number | BigNumber,
  web3Provider = null,
  contractKey = ERC721_HOLDER_CONTRACT
) => {
  return initContract(contractKey, true, web3Provider)
    .then(contract => contract.prepareClaim(claimInterface, rewardToken, rewardTokenId, nftId))
}

const prepareOffchainClaim = (
  claimInterface : string,
  rewardToken : string,
  rewardTokenId : number | BigNumber,
  nftId : number | BigNumber
) => {
  const integrityHash = utils.solidityKeccak256(["address", "uint256", "bytes4"], [rewardToken, rewardTokenId, claimInterface])
  const encNftId = utils.defaultAbiCoder.encode(["uint256"], [nftId])
  return utils.defaultAbiCoder.encode(["bytes32", "bytes"], [integrityHash, encNftId])
}

export {
  abi,
  bytecode,
  deployedAddress,
  owner,
  getReward,
  hasClaim,
  prepareClaim,
  prepareOffchainClaim
}
export default {
  abi,
  bytecode,
  deployedAddress,
  owner,
  getReward,
  hasClaim,
  prepareClaim,
  prepareOffchainClaim
}