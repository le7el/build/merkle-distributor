// We require the Hardhat Runtime Environment explicitly here. This is optional
// but useful for running the script in a standalone fashion through `node <script>`.
//
// When running the script with `npx hardhat run <script>` you'll find the Hardhat
// Runtime Environment's members available in the global scope.
const { ethers } = require("hardhat");

const TOKEN_ADDRESS = "0x914A428404657CA547085Ec0Ee7Ac6f948f99A4E"; // Seploia experience tokens minter
const ERC1155_MINT_INTERFACE = "0x731133e9";
const MERKLE_ROOT = "0x7a6d15ec717b2752460b077bb65d9bb427b20ef1c5d88bf9d088a8c2b7f82009";
const IPFS_CID = "0x6bc5822809da9b373f696e062e922f6eaa11cb8fd6cbdf5eed567577d817c811" // QmVbM1a5SLK73K6Wm1YMu7pC9otr59HpfR3i9pSXEdSk4G

async function main() {
  // Hardhat always runs the compile task when running scripts with its command
  // line interface.
  //
  // If this script is run directly using `node` you may want to call compile
  // manually to make sure everything is compiled
  // await hre.run('compile');

  // We get the contract to deploy
  const MerkleDistributorContract = await ethers.getContractFactory("MerkleDistributor");
  const MerkleDistributor = await MerkleDistributorContract.deploy(TOKEN_ADDRESS, 0, ERC1155_MINT_INTERFACE, MERKLE_ROOT, IPFS_CID);
  await MerkleDistributor.deployed();
  console.info("MerkleDistributor deployed to:", MerkleDistributor.address);

  // Verification
  const NETWORK = "live_sepolia"
  console.info("To verify your contracts on etherscan run the following commands:")
  console.info(`npx hardhat verify --network ${NETWORK} ${MerkleDistributor.address} ${TOKEN_ADDRESS} 0 ${ERC1155_MINT_INTERFACE} ${MERKLE_ROOT} ${IPFS_CID}`)
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
