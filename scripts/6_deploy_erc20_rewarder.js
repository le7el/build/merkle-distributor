// We require the Hardhat Runtime Environment explicitly here. This is optional
// but useful for running the script in a standalone fashion through `node <script>`.
//
// When running the script with `npx hardhat run <script>` you'll find the Hardhat
// Runtime Environment's members available in the global scope.
const { ethers } = require("hardhat");

const REWARD_TOKEN = "0xeA46Bf7Fe6c9a2e29a97D2Da2775131067b4BA0C" // "0xF59F671789FB2204b87f6c4405CDf2A6cdA585AA"
const MINTER = "0xeA46Bf7Fe6c9a2e29a97D2Da2775131067b4BA0C" // "0xF59F671789FB2204b87f6c4405CDf2A6cdA585AA"
const NFT_CONTRACT = "0x351cd141d53c2a7ca8fcadc90af2470469a8d1b1" //"0x68Fe087B98a42620A021AA51BAFAf3cB5952680F"
const AMOUNT_50 = "50000000000000000000000000"
const AMOUNT_100 = "100000000000000000000000000"
const AMOUNT_150 = "150000000000000000000000000"
const NFT_50_IDS = [
  "101576793338762700978690622431390172298363618431052578859104368944834259948410",
  "29998852015368824047581374654999997097710194376871282423591671936224736970930"
  //"14596531156220036522956221649226447327778061092334643925230946372690954504878", 
  //"35118203869140875433437411372119932450999467886854184603900344553710577265549"
]
const NFT_50_LEVELS = [10, 10]
const NFT_100_IDS = [
  "12690401128846813410385437332181969362396771455017847924461100840017687607264"
  //"49368134766074312367939166193989180587074579594038058741697515945441416017844"
]
const NFT_100_LEVELS = [40]

async function main() {
  // Hardhat always runs the compile task when running scripts with its command
  // line interface.
  //
  // If this script is run directly using `node` you may want to call compile
  // manually to make sure everything is compiled
  // await hre.run('compile');

  const ERC20RewarderContract = await ethers.getContractFactory("ERC20Rewarder");
  const ERC20Rewarder = await ERC20RewarderContract.deploy();
  await ERC20Rewarder.deployed();
  console.info("ERC20Rewarder deployed to:", ERC20Rewarder.address);

  const now = Math.floor(Date.now() / 1000);
  
  const FixedVirtualDistributorContract = await ethers.getContractFactory("FixedVirtualDistributor");
  const FixedVirtualDistributor50 = await FixedVirtualDistributorContract.deploy(
    now,
    now + 1,
    ERC20Rewarder.address,
    AMOUNT_50,
    NFT_CONTRACT,
    NFT_50_IDS,
    NFT_50_LEVELS
  );
  await FixedVirtualDistributor50.deployed();
  console.info("FixedVirtualDistributor 50m deployed to:", FixedVirtualDistributor50.address);

  const FixedVirtualDistributor100 = await FixedVirtualDistributorContract.deploy(
    now,
    now + 63113904, // 2 years
    ERC20Rewarder.address,
    AMOUNT_100,
    NFT_CONTRACT,
    NFT_100_IDS,
    NFT_100_LEVELS
  );
  await FixedVirtualDistributor100.deployed();
  console.info("FixedVirtualDistributor 100m deployed to:", FixedVirtualDistributor100.address);

  await ERC20Rewarder.immutableConfig(
    REWARD_TOKEN,
    FixedVirtualDistributor50.address,
    FixedVirtualDistributor100.address,
    "0x40c10f19",
    MINTER,
    AMOUNT_150
  );

  // Verification
  const NETWORK = "live_mainnet"
  console.info("To verify your contracts on etherscan run the following commands:")
  console.info(`npx hardhat verify --network ${NETWORK} ${ERC20Rewarder.address}`)
  console.info(`npx hardhat verify --network ${NETWORK} ${FixedVirtualDistributor50.address} ${now} ${now + 1} ${ERC20Rewarder.address} ${AMOUNT_50} ${NFT_CONTRACT} ${JSON.stringify(NFT_50_IDS)} ${JSON.stringify(NFT_50_LEVELS)}`)
  console.info(`npx hardhat verify --network ${NETWORK} ${FixedVirtualDistributor100.address} ${now} ${now + 63115200} ${ERC20Rewarder.address} ${AMOUNT_100} ${NFT_CONTRACT} ${JSON.stringify(NFT_100_IDS)} ${JSON.stringify(NFT_100_LEVELS)}`)
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});