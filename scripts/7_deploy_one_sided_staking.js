// We require the Hardhat Runtime Environment explicitly here. This is optional
// but useful for running the script in a standalone fashion through `node <script>`.
//
// When running the script with `npx hardhat run <script>` you'll find the Hardhat
// Runtime Environment's members available in the global scope.
const { ethers } = require("hardhat");

const STAKING_TOKEN_ADDRESS = "0x84a431Bd2C958414B2E316CEdd9F85993ace5000" // "0xF59F671789FB2204b87f6c4405CDf2A6cdA585AA" // Sepolia L7L token

async function main() {
  // Hardhat always runs the compile task when running scripts with its command
  // line interface.
  //
  // If this script is run directly using `node` you may want to call compile
  // manually to make sure everything is compiled
  // await hre.run('compile');

  const OneSidedStakingContract = await ethers.getContractFactory("OneSidedStaking");
  const OneSidedStaking = await OneSidedStakingContract.deploy(STAKING_TOKEN_ADDRESS, STAKING_TOKEN_ADDRESS);
  await OneSidedStaking.deployed();
  console.info("OneSidedStaking deployed to:", OneSidedStaking.address);

  // Verification
  const NETWORK = "live_polygon" //"live_sepolia"
  console.info("To verify your contracts on etherscan run the following commands:")
  console.info(`npx hardhat verify --network ${NETWORK} ${OneSidedStaking.address} ${STAKING_TOKEN_ADDRESS} ${STAKING_TOKEN_ADDRESS}`)
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});