// We require the Hardhat Runtime Environment explicitly here. This is optional
// but useful for running the script in a standalone fashion through `node <script>`.
//
// When running the script with `npx hardhat run <script>` you'll find the Hardhat
// Runtime Environment's members available in the global scope.
const { ethers } = require("hardhat");

const ERC1155_MINT_INTERFACE = "0x731133e9";
const TOKEN_ADDRESS = "0x914A428404657CA547085Ec0Ee7Ac6f948f99A4E"; // Sepolia le7el experience token
const CONDITIONAL_DISTRIBUTOR = '0xC972af85DbD3d770D5E8668e1f81Fd45D52D2aaa' // Sepolia Conditional distributor
const VALIDATOR = '0x11169009E2E4956205632177ba1d2F2603342D91' // Test validator

async function main() {
  // Hardhat always runs the compile task when running scripts with its command
  // line interface.
  //
  // If this script is run directly using `node` you may want to call compile
  // manually to make sure everything is compiled
  // await hre.run('compile');

  const OneTimeOffchainTicketsContract = await ethers.getContractFactory("OneTimeOffchainTickets");
  const OneTimeOffchainTickets = await OneTimeOffchainTicketsContract.deploy(ERC1155_MINT_INTERFACE, TOKEN_ADDRESS, 0, CONDITIONAL_DISTRIBUTOR);
  await OneTimeOffchainTickets.deployed();
  console.info("OneTimeOffchainTickets deployed to:", OneTimeOffchainTickets.address);

  await OneTimeOffchainTickets.adminChangeValidator(VALIDATOR);
  const ConditionalDistributor = await ethers.getContractAt('ConditionalDistributor', CONDITIONAL_DISTRIBUTOR)
  await ConditionalDistributor.adminSwitchOracle(OneTimeOffchainTickets.address, true)

  // Verification
  const NETWORK = "live_sepolia"
  console.info("To verify your contracts on etherscan run the following commands:")
  console.info(`npx hardhat verify --network ${NETWORK} ${OneTimeOffchainTickets.address} ${ERC1155_MINT_INTERFACE} ${TOKEN_ADDRESS} 0 ${CONDITIONAL_DISTRIBUTOR}`)
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});