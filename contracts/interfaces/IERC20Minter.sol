// SPDX-License-Identifier: MPL-2.0
pragma solidity ^0.8.17;

/**
 * @dev ERC20 token with minting interface.
 */
interface IERC20Minter {
    /**
     * @dev Mint amount of tokens to the address for ERC20 token standard.
     * @param _to Address of token reciever.
     * @param _amount Amount of minted tokens.
     */
    function mint(address _to, uint256 _amount) external;
}