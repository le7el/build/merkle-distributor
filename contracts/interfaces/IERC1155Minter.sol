// SPDX-License-Identifier: MPL-2.0
pragma solidity ^0.8.17;

/**
 * @dev ERC1155 token with minting interface.
 */
interface IERC1155Minter {
    /**
     * @dev Mint amount of tokens to the address for ERC1155 multi-token standard.
     * @param _to Address of token reciever.
     * @param _id Token id which is minted.
     * @param _amount Amount of minted tokens.
     * @param _data Metadata.
     */
    function mint(address _to, uint256 _id, uint256 _amount, bytes memory _data) external;
}