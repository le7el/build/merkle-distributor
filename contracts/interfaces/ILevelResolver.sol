// SPDX-License-Identifier: MPL-2.0
pragma solidity ^0.8.17;

/**
 * @dev CRS resolver with leveling feature {see https://gitlab.com/le7el/play/crs-nfts/-/blob/master/src/resolver/LevelingResolver.sol}.
 */
interface ILevelResolver {
    /**
     * @dev Level based on experience.
     * @param _project node for a project which issue experience.
     * @param _node the node to query.
     * @return level based on experience
     */
    function level(bytes32 _project, bytes32 _node) external view returns (uint256);
}
