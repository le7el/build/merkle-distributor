// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.8.17;

/**
 * @dev Allows anyone to claim a token if they exist in a merkle root.
 */
interface IMerkleDistributor {
    /**
     * @dev Returns the address of the token distributed by this contract.
     * @return token contract address.
     */
    function token() external view returns (address);

    /**
     * @dev Returns the merkle root of the merkle tree containing account balances available to claim.
     * @return merkle root.
     */
    function merkleRoot() external view returns (bytes32);

    /**
     * @dev Returns true if the index has been marked claimed.
     * @return true if claim is no longer valid.
     */
    function isClaimed(uint256 index) external view returns (bool);

    /**
     * @dev Claim the given amount of the token to the given address. Reverts if the inputs are invalid.
     * @param index merkle tree index for the claim.
     * @param account reward beneficiary address.
     * @param merkleProof claim if form of merkle proof.
     */
    function claim(uint256 index, address account, uint256 amount, bytes32[] calldata merkleProof) external;

    // This event is triggered whenever a call to #claim succeeds.
    event Claimed(uint256 index, uint256 indexed round, address indexed account, uint256 amount);
}