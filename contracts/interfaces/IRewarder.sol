// SPDX-License-Identifier: MPL-2.0

pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";

/**
 * @dev Contract responsible for reward minting / transfers.
 */
interface IRewarder {
    /**
     * @dev Method where reward minting or transfer should happen.
     * @param nftContract The NFT contract of the pool.
     * @param tokenId NFT token id.
     * @param user Address which triggers reward distribution
     * @param recipient reward beneficiary address.
     * @param amount pending reward.
     * @param newLpAmount total LP tokens which recieve reward.
     */
    function onReward(
        address nftContract,
        uint256 tokenId,
        address user,
        address recipient,
        uint256 amount,
        uint256 newLpAmount
    ) external;

    /**
     * @dev Pending rewards in different tokens which are to be distributed to LP holders.
     * @param nftContract The NFT contract of the pool.
     * @param tokenId NFT token id.
     * @param amount LP token amount.
     * @return tokens to be rewarded.
     * @return amounts to be rewarded.
     */
    function pendingTokens(
        address nftContract,
        uint256 tokenId,
        uint256 amount
    ) external view returns (IERC20[] memory, uint256[] memory);
}