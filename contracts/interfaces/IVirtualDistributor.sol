// SPDX-License-Identifier: MPL-2.0
pragma solidity ^0.8.17;

/**
 * @dev Distribute rewards in form of NFT metadata updates.
 */
interface IVirtualDistributor {
    /**
     * @dev Amount of unclaimed rewards pending for specific NFT.
     * @param _nftContract The NFT contract of the pool.
     * @param _tokenId NFT token id.
     * @return pending reward in tokens.
     */
    function pendingRewards(address _nftContract, uint256 _tokenId) external view returns (uint256);

    /**
     * @dev Checks if rewards can't be immediatly claimed from specific NFT.
     *      It's used to prevent NFT sale scams where reward is claimed
     *      just before the sale is performed confusing the buyer.
     * @param _nftContract The NFT contract of the pool.
     * @param _tokenId NFT token id.
     * @return true if rewards are safely locked.
     */
    function isRewardSafelyLocked(address _nftContract, uint256 _tokenId) external view returns (bool);
}