// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.8.17;

import '@openzeppelin/contracts/token/ERC1155/ERC1155.sol';

contract MockERC1155 is ERC1155 {
    constructor () ERC1155("https://token.uri/{id}") {}

    function mint(address _to, uint _id, uint _amount, bytes calldata _data) public {
        _mint(_to, _id, _amount, _data);
    }
}