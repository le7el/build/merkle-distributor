// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.8.17;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import {IRewarder} from "../interfaces/IRewarder.sol";
import {IERC20Minter} from "../interfaces/IERC20Minter.sol";

contract MockRewarder is IRewarder {
  IERC20 public token;
  
  constructor(IERC20 _token) {
      token = _token;
  }

  function onReward(
      address, //nftContract,
      uint256, //tokenId,
      address, //user,
      address recipient,
      uint256 amount,
      uint256 //newLpAmount
  ) external {
      IERC20Minter(address(token)).mint(recipient, amount);
  }

  function pendingTokens(
      address,
      uint256,
      uint256
  ) external view returns (IERC20[] memory, uint256[] memory) {
      IERC20[] memory _tokens;
      _tokens[0] = token;
      uint256[] memory _amounts;
      _amounts[0] = 0;

      return (_tokens, _amounts);
  }
}
